FROM node:14-buster

# Tell Puppeteer to skip installing Chrome. We'll be using the installed package.
ENV PUPPETEER_SKIP_CHROMIUM_DOWNLOAD=true

ARG PA11Y_CI_VERSION=2

RUN apt-get update \
    && apt-get install -y \
        apt-transport-https \
        ca-certificates \
        chromium \
        curl \
        fontconfig \
        fonts-ipafont-gothic \
        fonts-kacst \
        fonts-liberation \
        fonts-thai-tlwg \
        fonts-wqy-zenhei \
        gconf-service \
        libappindicator1 \
        libasound2 \
        libatk1.0-0 \
        libc6 \
        libcairo2 \
        libcups2 \
        libdbus-1-3 \
        libexpat1 \
        libfontconfig1 \
        libgcc1 \
        libgconf-2-4 \
        libgdk-pixbuf2.0-0 \
        libglib2.0-0 \
        libgtk-3-0 \
        libnspr4 \
        libnss3 \
        libpango-1.0-0 \
        libpangocairo-1.0-0 \
        libstdc++6 \
        libx11-6 \
        libx11-xcb1 \
        libxcb1 \
        libxcomposite1 \
        libxcursor1 \
        libxdamage1 \
        libxext6 \
        libxfixes3 \
        libxi6 \
        libxrandr2 \
        libxrender1 \
        libxss1 \
        libxtst6 \
        locales \
        lsb-release \
        unzip \
        xdg-utils \
        wget \
    && apt-get autoremove -y -q \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* /usr/share/doc /usr/share/man /tmp/*

RUN yarn global add \
        pa11y-ci@${PA11Y_CI_VERSION}.* \
        depcheck \
        --unsafe-perm \
    && yarn cache clean

WORKDIR /workspace
